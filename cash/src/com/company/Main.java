package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Сколько ты зарабатываешь?(в час,указать в $): ");
        int money = in.nextInt();
        if (money < 8) {
            System.out.println("Нам вас очень жаль,вы не можете получать меньше 8$,поэтому вы будете получать 8$");
            money = 8;
        }
        System.out.println("Сколько вы работаете?(в часах в неделю): ");

        int hours = in.nextInt();
        if (hours > 60) {
            System.out.println("Не переутруждайте себя,максимальное количество рабочих часов в неделю - 60");
            hours = 60;
        }
        double general = 0;    //общая заработанная сумма
        double cf = 1;         // коэф на который мы будем умножать заработок
        int i = 1;
        while (i <= hours){
            general += (cf * money);
            if (i == 40)
                cf = 1.5;
            i++;
        }
        System.out.println("Поздравляем,ваш заработок составил(барабанная дробь): "+general+"$");
    }
}
